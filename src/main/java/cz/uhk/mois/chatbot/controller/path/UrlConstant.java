package cz.uhk.mois.chatbot.controller.path;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author Jan Krunčík
 * @since 26.03.2020 9:40
 */

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UrlConstant {

    public static final String CHAT_BOT = "/chatbot";
    public static final String CHAT_BOT_QUESTION = "/question";
}
