package cz.uhk.mois.chatbot.service;

import cz.uhk.mois.chatbot.dto.QuestionDtoIn;
import cz.uhk.mois.chatbot.dto.QuestionDtoOut;
import cz.uhk.mois.chatbot.response.Failure;
import cz.uhk.mois.chatbot.response.Success;
import io.vavr.control.Either;

/**
 * @author Jan Krunčík
 * @since 26.03.2020 20:09
 */

public interface ChatBotService {

    /**
     * Getting the answer to the question. <br/>
     * <i>Ask a chat bot</i>
     *
     * @param dtoIn
     *         input data with a question to chat bot
     *
     * @return right with the answer to the question or left with information about the error
     */
    Either<Failure, Success<QuestionDtoOut>> getAnswer(QuestionDtoIn dtoIn);
}
