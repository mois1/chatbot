package cz.uhk.mois.chatbot.validation;

import cz.uhk.mois.chatbot.dto.QuestionDtoIn;
import io.vavr.control.Validation;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * @author Jan Krunčík
 * @since 26.03.2020 19:44
 */

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Log4j2
public class QuestionDtoInValidator {

    public static Validation<String, QuestionDtoIn> validate(QuestionDtoIn dtoIn) {
        log.info("Validate question. {}", dtoIn);
        return QuestionValidationSupport.validateQuestion(dtoIn.getQuestion())
                                        .mapError(validationViolation -> validationViolation)
                                        .map(QuestionDtoIn::new);
    }
}
