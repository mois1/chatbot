package cz.uhk.mois.chatbot.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Jan Krunčík
 * @since 26.03.2020 9:43
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class QuestionDtoIn {

    private String question;
}
